package com.sha.sprintbootmicroservicegateway.service;

import com.sha.sprintbootmicroservicegateway.model.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    public User saveUser(User user);

    List<User> findAllUsers();

    Optional<User> findBUsername(String username);
}
