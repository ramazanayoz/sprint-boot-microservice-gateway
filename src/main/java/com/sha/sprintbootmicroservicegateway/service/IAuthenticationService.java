package com.sha.sprintbootmicroservicegateway.service;

import com.sha.sprintbootmicroservicegateway.model.User;

public interface IAuthenticationService {
    String signInAndReturnJWT(User signInRequest);
}
